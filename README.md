# Список контактов

## Описание
Это приложение предназначено для управления списком контактов. Оно позволяет добавлять, редактировать, просматривать и удалять контакты. Интерфейс пользователя реализован с помощью Thymeleaf, а обработка запросов осуществляется через контроллеры Spring MVC. Работа с базой данных происходит через JdbcTemplate.

## Технологии
- Spring MVC
- Spring Data JDBC
- Thymeleaf
- H2 Database
- Docker

## Запуск приложения

### Требования
- JDK 11+
- Docker

### Шаги для запуска
1. Клонируйте репозиторий:
    ```bash
    git clone https://gitlab.com/java-spring-part-two/3.8.git
    cd demo
    ```

2. Запустите контейнер базы данных через Docker:
    ```bash
    docker run --name contactdb -e POSTGRES_DB=contacts -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password -p 5432:5432 -d postgres
    ```

3. Измените настройки базы данных в `src/main/resources/application.properties`, если это необходимо:
    ```properties
    spring.datasource.url=jdbc:postgresql://localhost:5432/contacts
    spring.datasource.username=postgres
    spring.datasource.password=password
    ```

4. Соберите и запустите приложение:
    ```bash
    ./mvnw spring-boot:run
    ```

5. Откройте браузер и перейдите по адресу [http://localhost:8080](http://localhost:8080), чтобы увидеть список контактов.

## Команды
- **Создать контакт**: Перейдите по ссылке "Добавить контакт" и заполните форму.
- **Редактировать контакт**: Нажмите "Редактировать" рядом с контактом, который хотите изменить, и обновите информацию.
- **Удалить контакт**: Нажмите "Удалить" рядом с контактом, который хотите удалить.

## Примечания
- Форма создания и редактирования контактов общая.
- ID контакта не изменяется через UI.
