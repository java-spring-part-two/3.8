package com.example.demo.controller;

import com.example.demo.model.Contact;
import com.example.demo.repository.ContactRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class ContactController {
    
    private final ContactRepository repository;

    public ContactController(ContactRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/")
    public String showContactList(Model model) {
        model.addAttribute("contacts", repository.findAll());
        return "index";
    }

    @GetMapping("/add")
    public String showAddForm(Contact contact) {
        return "add-contact";
    }

    @PostMapping("/add")
    public String addContact(@ModelAttribute Contact contact) {
        repository.save(contact);
        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Contact contact = repository.findById(id)
        .orElseThrow(() -> new IllegalArgumentException("Invalid contact Id:" + id));
        model.addAttribute("contact", contact);
        return "add-contact";
    }

    @PostMapping("/update/{id}")
    public String updateContact(@PathVariable("id") long id, @ModelAttribute Contact contact) {
        contact.setId(id);
        repository.save(contact);
        return "redirect:/";
    }

    @GetMapping("/delete/{id}")
    public String deleteContact(@PathVariable("id") long id) {
        Contact contact = repository.findById(id)
        .orElseThrow(() -> new IllegalArgumentException("Invalid contact Id:" + id));
        repository.delete(contact);
        return "redirect:/";
    }
}
